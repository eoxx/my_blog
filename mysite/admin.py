from django.contrib import admin

from mysite.models import (User, ArticleTag,
                           ArticleCategory, Article)


# @admin.register(User)
class UserAdmin(admin.ModelAdmin):
    '''
    装饰器 和 函数注册是一样的效果
    '''
    # 设置在后台中展示的字段
    list_display = ('username', 'phone', 'email', 'nickname', 'date_joined')


admin.site.register(User, UserAdmin)


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date', 'author', 'views')
    filter_horizontal = ('tags',)


@admin.register(ArticleTag)
class ArticleTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_date')
    exclude = ('slug',)


@admin.register(ArticleCategory)
class ArticleCategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_date')

