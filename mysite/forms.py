from django import forms

#登录表单，
class LoginForm(forms.Form):
    # 用户名
    username = forms.CharField(
        # 使用的是html中的文本输入库，经过Django 转换后是input标签
        widget=forms.TextInput(attrs={
            'placeholder': '请输入用户名'  # 添加属性
        }),
        max_length=25, # 最大
        required=True, # 必须
        error_messages={'required': "用户名不能为空"}, # 用户输入有误的提示
        )

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': '请输入密码',
    }))
