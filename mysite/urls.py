from django.urls import path
from .views import index, ArticleDetailView, about,logout_, login_


urlpatterns = [
    path('', index),
    path('detail/<int:id>', ArticleDetailView.as_view(), name='detail'),
    path('about/', about, name='about'),
    path('login/', login_, name='login'),
    path('logout/', logout_, name='logout'),
]
