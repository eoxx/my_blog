from django.shortcuts import render,redirect
from django.contrib import auth

from django.views.generic import DetailView
from mysite.models import Article

from mysite.forms import LoginForm


def index(request):
    articles = Article.objects.filter(status=1)
    return render(request, 'index.html', locals())


class ArticleDetailView(DetailView):
    template_name = 'detail.html'
    model = Article
    pk_url_kwarg = 'id'


def about(request):
    return render(request, 'about.html')

# 登录
def login_(request):
    # 如果是post请求
    if request.method == 'POST':
        # 接收请求参数
        form = LoginForm(request.POST)
        # 验证表单是否有效
        if form.is_valid():
            # 查询数据库是否由此用户，以及密码是否匹配， 此处使用的是django自带的系统
            # 也可自己编写
            user = auth.authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user:
                # 登录
                auth.login(request, user)
                return redirect('/')

            error = '用户名或密码错误'
            return render(request, 'login.html', locals())


    form = LoginForm()
    return render(request, 'login.html', locals())


def logout_(request):
    auth.logout(request)
    return redirect('/')