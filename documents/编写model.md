编写博客的相关model。

models.py
### 文章分类
```python
class ArticleCategory(models.Model):
    '''文章分类'''
    name = models.CharField('分类名称', max_length=20)
    created_date = models.DateTimeField('创建时间', auto_now_add=True)

    class Meta:
        verbose_name = '文章分类'
        # 展示在后台中的名称
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
```

### 文章标签
```python
class ArticleTag(models.Model):
    '''文章标签'''
    name = models.CharField('标签名称', max_length=20)
    slug = models.SlugField(default='no-slug', max_length=60, blank=True)
    created_date = models.DateTimeField('创建时间', auto_now_add=True)

    class Meta:
        verbose_name = '标签'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('blog:tag_detail', kwargs={'tag_name': self.slug})

```

### 文章
```python
class Article(models.Model):
    '''文章'''
    STATUS_CHOICES = (
        (0, '草稿'),
        (1, '发表'),
    )

    title = models.CharField('文章标题', max_length=50, null=False)
    content = models.TextField('文章内容', null=False)

    pub_date = models.DateTimeField('发布时间', auto_now_add=True)
    last_mod_date = models.DateTimeField('最后修改时间', auto_now_add=True)

    author = models.ForeignKey(User, verbose_name='作者', on_delete=models.CASCADE)
    status = models.IntegerField('文章状态', choices=STATUS_CHOICES, default=0)
    category = models.ForeignKey( ArticleCategory, verbose_name='分类', on_delete=models.CASCADE, blank=False, null=False)

    views = models.IntegerField('浏览量', default=0)

    tags = models.ManyToManyField(ArticleTag, verbose_name='标签', blank=True)

    # 元数据，
    class Meta:
        # 文章排序规则
        ordering = ['-pub_date']
        verbose_name = "文章"
        verbose_name_plural = verbose_name
        get_latest_by = 'id'
```

### 注册到Admin后台
```python
from django.contrib import admin

from mysite.models import (User, ArticleTag,
                           ArticleCategory, Article)


# @admin.register(User)
class UserAdmin(admin.ModelAdmin):
    '''
    装饰器 和 函数注册是一样的效果
    '''
    # 设置在后台中展示的字段
    list_display = ('username', 'phone', 'email', 'nickname', 'date_joined')


admin.site.register(User, UserAdmin)

# 以下是新增

# 文章
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    # 展示的字段
    list_display = ('title', 'pub_date', 'author', 'views')
    # 添加文章的时候，选择标签 横排显示
    filter_horizontal = ('tags',)


@admin.register(ArticleTag)
class ArticleTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_date')
    # 不显示在后台
    exclude = ('slug',)


@admin.register(ArticleCategory)
class ArticleCategoryAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_date')

```

### 同步数据库
```bash
$ python3 manage.py makemigrations
$ python3 manage.py migrate
```
将会在mysite/migrations中生成数据库变动的记录。  

启动服务器，浏览器中输入`http://127.0.0.1/admin`，使用前面创建的超级用户登录进去。  
![](https://oss.litets.com/site_WechatIMG11.png)  

在后台中可以管理`文章` `文章分类` `标签` `用户`等。

首先我们在`文章分类` `标签`中添加Python类型。  

![](https://oss.litets.com/site-WechatIMG12.png)  

点击文章-新增，添加有一篇新博客。  
![](https://oss.litets.com/site-WechatIMG13.png)  

![](https://oss.litets.com/site-WechatIMG14.png)  
